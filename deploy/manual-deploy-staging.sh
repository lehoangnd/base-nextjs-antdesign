#!/usr/bin/env bash

USER=root
HOST=103.90.xxx
PORT=22
ENV=$1
APP_NAME=base-nextjs

# server dependencies: node version 14x
echo "=========> Start Deploy ${ENV}"
npm install

npm run copy:env.${ENV}

npm run build

cp -r ./deploy/.htaccess ./out/.htaccess

tar -cf build.tar.gz ./out ./deploy/apache/${APP_NAME}-${ENV}.conf

scp -P ${PORT} -o StrictHostKeyChecking=no ./build.tar.gz "${USER}@${HOST}":~
ssh -p ${PORT} -o StrictHostKeyChecking=no "${USER}@${HOST}" "bash -s" < ./deploy/deploy.sh "${ENV}"

rm -rf ./build.tar.gz
echo "=========> End Deploy ${ENV}"