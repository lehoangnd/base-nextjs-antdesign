# Tạo thư mục chứa dự án

APP_NAME=base-nextjs

sudo mkdir -p /var/www/html/$APP_NAME-$1
cd /var/www/html/$APP_NAME-$1
sudo rm -rf build
sudo tar -xf ~/build.tar.gz -C .
## copy config: chạy lần đầu
cp -r deploy/apache/$APP_NAME-$1.conf /etc/httpd/conf.d/$APP_NAME-$1.conf
## restart apache | httpd
service httpd restart

# xóa file compress & deploy folder
rm ~/build.tar.gz
rm -rf deploy