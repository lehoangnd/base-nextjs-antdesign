export class TextUtils {
    static isEmpty(text?: string): boolean {
        if (!text) return true;

        if (text.trim() === '') return true;

        return false;
    }

    static formatName = (str: string): string => {
        return str.replace(/\s{2,}/g, ' ').trim();
    };

    static removeDialCodePhone = (phone: string, dialCode: string): string => {
        return phone
            .replace(/ /g, '')
            .replace(dialCode, '')
            .replace(/^0/, '')
            .trim();
    };
    static _formatPrice = (num: string | number) => {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };

    /**
     * Chuyển đổi ID sang :id
     * Ví dụ: edit/staff/5fc852d82036c454f8a57a05 -> edit/staff/:id
     * @param text
     */
    static transformIdPath = (text: string) => {
        return text.replace(/[0-9a-fA-F]{24}$/, ':id');
    };

    static formatNumberPrice = (number: string | number = '') => {
        return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
}
