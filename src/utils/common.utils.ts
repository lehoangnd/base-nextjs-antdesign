import EventBus from './event-bus/event-bus';
import { EventBusName } from './event-bus/event-bus.types';
import { message } from 'antd';
import { translates } from 'res/languages';
import moment from 'moment';

export const showModalSignup = (): void => {
    EventBus.getInstance().post({
        type: EventBusName.SHOW_SIGNUP_EVENT
    });
};

export const showModalSignin = (): void => {
    EventBus.getInstance().post({
        type: EventBusName.SHOW_SIGNIN_EVENT
    });
};

export const showModalForgotPassword = (): void => {
    EventBus.getInstance().post({
        type: EventBusName.SHOW_FORGOT_PASSWORD_EVENT
    });
};

export const eventBusLogout = (): void => {
    EventBus.getInstance().post({
        type: EventBusName.LOGOUT_EVENT
    });
};

export const showModalOtp = (payload: {
    phone?: string;
    token?: string;
    dialCode?: string;
    email?: string;
    password?: string;
    name?: string;
}): void => {
    EventBus.getInstance().post({
        type: EventBusName.SHOW_OTP_EVENT,
        payload
    });
};

export const showModalChangePassword = (payload: { token: string }): void => {
    EventBus.getInstance().post({
        type: EventBusName.SHOW_CHANGE_PASSWORD_EVENT,
        payload
    });
};

export const eventAuthSuccess = (): void => {
    EventBus.getInstance().post({
        type: EventBusName.AUTH_SUCCESS_EVENT
    });
};

export const showLoading = (content: string, key?: string): void => {
    message.loading({
        content: translates(content),
        key: key || 'helper'
    });
};

export const showAlertSuccess = (content: string, key?: string): void => {
    message.success({
        content: translates(content),
        key: key || 'helper',
        style: { marginTop: '15vh' }
    });
};

export const showAlertError = (content: string, key?: string): void => {
    message.error({
        content: translates(content),
        key: key || 'helper',
        style: { marginTop: '15vh' }
    });
};

const imageMime = ['image/jpeg', 'image/png', 'image/jpg'];
export const isImageFile = (type: string): boolean => {
    return imageMime.includes(type);
};

export const customFormat = (value: any) => {
    if (value) {
        return translates('displayDateFormat', {
            day: moment(value).format('DD'),
            month: moment(value).format('MM'),
            year: moment(value).format('YYYY')
        });
    }
};
export const getStringAfterCharacter = (
    str?: string | string[] | undefined,
    key: string = '-'
): string => {
    if (!str) return '';
    if (typeof str === 'object')
        return str[0].slice(str[0].lastIndexOf(key) + 1);
    return str.slice(str.lastIndexOf(key) + 1);
};

export const newsTime = (value: string): string => {
    return moment(value).format('DD/MM/YYYY');
};

export const capitalizeEachWord = (str: string): string => {
    const content: string = translates(str);
    const newStr = content.split(' ').map(s => s.charAt(0).toUpperCase() + s.slice(1)).join(' ');
    return newStr;
}

export const numberWithCommas = (number: number | string) => {
    return (
        (number &&
            number
                .toString()
                .replace(/[^0-9\.]+/g, '')
                .replace(/\B(?=(\d{3})+(?!\d))/g, ',')) ||
        number
    );
};


export const markSelectHtml = (str: string, char1: string, char2: string) => {
    let re = `\\${char1}(.*?)\\${char2}`
    let reg = new RegExp(re, 'gi');
    return str.replace(reg, function (match, contents, offset, input_string) {
        const number = getStringAfterCharacter(contents, '#')
        const arr = contents.replace(`#${number}`, '').split('/');
        let sl = `<select id="${number}">`;
        arr.forEach((e: string, i: number) => {
            
            if (i === arr.length - 1) {
                sl += `<option>${e}</option></select>`;
            } else {
                sl += `<option>${e}</option>`
            }
        })
        return sl;
    })
}