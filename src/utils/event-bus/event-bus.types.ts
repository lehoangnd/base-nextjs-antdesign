export interface BaseEvent {
  type: EventBusName;
}

export interface BaseEventPayload<Payload> {
  type: EventBusName;
  payload: Payload;
}

export enum EventBusName {
  LOGOUT_EVENT,
  SHOW_SIGNUP_EVENT,
  SHOW_SIGNIN_EVENT,
  AUTH_SUCCESS_EVENT,
  SHOW_FORGOT_PASSWORD_EVENT,
  SHOW_CHANGE_PASSWORD_EVENT,
  SHOW_OTP_EVENT,
  NO_TOKEN_EVENT,
  FETCH_TOTAL_ORDER_EVENT,
}
