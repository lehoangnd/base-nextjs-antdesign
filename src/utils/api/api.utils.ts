import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { TokenInfor } from 'constants/global-variable';
import { AsyncStorageUtils, StorageKey } from '../async-storage.utils';
import EventBus from '../event-bus/event-bus';
import { EventBusName } from '../event-bus/event-bus.types';
import { ResCode } from './api.types';
import Router from 'next/router';
import { translates } from 'res/languages';
import { showAlertError } from 'utils/common.utils';

interface CustomHeaders {
    isAuth: boolean;
}

const REQ_TIMEOUT = 15 * 1000;
export const __DEV__ =
    !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

const instance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_DOMAIN,
    timeout: REQ_TIMEOUT
});

instance.interceptors.request.use((_config) => requestHandler(_config));

const requestHandler = (request: AxiosRequestConfig) => {
    if (__DEV__) {
        console.log(
            `Request API: ${request.url}`,
            request.params,
            request.data
        );
    }

    return request;
};

instance.interceptors.response.use(
    (response) => successHandler(response),
    (error) => errorHandler(error)
);

const errorHandler = (error: any) => {
    return Promise.reject({ ...error });
};

const successHandler = async (response: AxiosResponse) => {
    if (__DEV__) {
        console.log(`Response API: ${response.config.url}`, response.data);
    }
    const originalRequest = response.config;

    const data: any = response.data;
    //@ts-ignore
    if (data.status === 'INVALID_TOKEN' && !originalRequest._retry) {
        //@ts-ignore
        originalRequest._retry = true;
        const tokenInfor: TokenInfor | null = AsyncStorageUtils.getObject(
            StorageKey.TOKEN
        );

        if (!tokenInfor) {
            showAlertError('loginExpiered');
            EventBus.getInstance().post({
                type: EventBusName.NO_TOKEN_EVENT,
            });
            Router.push('/');
            return;
        }
        const refreshRes: any = await instance.post('/auth/refresh-token', {
            refreshToken: `Bearer ${tokenInfor.refreshToken}`
        });
        if (refreshRes.status === ResCode.SUCCESS) {
            const tokenInfor: TokenInfor = {
                token: data.token,
                refreshToken: data.refreshToken,
                expiresAt: data.expiresAt
            };
            AsyncStorageUtils.saveObject(StorageKey.TOKEN, tokenInfor);

            originalRequest.headers['Authorization'] =
                'Bearer ' + refreshRes.token;
            return instance(originalRequest);
        }
    }

    return data;
};

async function fetch<ReqType, ResType>(
    url: string,
    params?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.get(url, { params, headers });
}

async function post<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, { ...data }, { headers });
}

async function postForm<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, data, { headers: { ...headers, 'Content-Type': 'multipart/form-data' } });
}

async function put<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.put(url, { ...data }, { headers });
}

async function remove<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders?: CustomHeaders
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.delete(url, { data: { ...data }, headers: { ...headers } });
}

function getHeader(customHeaders?: CustomHeaders): any {
    const header: any = customHeaders || {};
    const tokenInfor: TokenInfor | null = AsyncStorageUtils.getObject(
        StorageKey.TOKEN
    );
    if (tokenInfor) {
        header.Authorization = `Bearer ${tokenInfor.token}`;
    }

    return { ...header };
}

const ApiUtils = { fetch, post, put, postForm, remove };
export default ApiUtils;
