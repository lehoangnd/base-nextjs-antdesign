import firebase from 'firebase/app';
import "firebase/messaging";
import 'firebase/auth'
import { showAlertError } from './common.utils';



const firebaseConfig = process.env.NEXT_PUBLIC_NODE_ENV === 'staging' ? {
    // xảy ra lỗi với message khi dùng config từ env
    apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
    authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
    projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
    storageBucket: process.env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.NEXT_PUBLIC_FIREBASE_MESSAGE_SENDER_ID,
    appId: process.env.NEXT_PUBLIC_FIREBASE_APPID

    // apiKey: "",
    // authDomain: "",
    // projectId: "",
    // storageBucket: "",
    // messagingSenderId: "",
    // appId: ""
} : {};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}
firebase.auth().useDeviceLanguage();

const firebaseAuth = firebase.auth();

// function signin
async function signInWithPopup(
    provider: firebase.auth.AuthProvider
): Promise<any> {
    return await firebaseAuth
        .signInWithPopup(provider)
        .then((result: firebase.auth.UserCredential) => {
            const credential = result.credential;
            //@ts-ignore
            return credential!.accessToken;
        })
        .catch((error) => {
            showAlertError('loginFbFailure');
            return undefined;
        });
};

// đăng nhập với facebook
export async function loginWithFacebook(): Promise<string | undefined> {
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.setCustomParameters({
        display: 'popup'
    });

    return await signInWithPopup(provider);
}

// đăng nhập với google
export async function loginWithGoogle(): Promise<string | undefined> {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/userinfo.email');

    return await signInWithPopup(provider);
}

// check đăng nhập
async function checkLoginSection() {
    const user = firebase.auth().currentUser;
    if (user) {
        await firebaseAuth.signOut()
    }
}

// gửi mã OTP
export async function sendOTP(phone: string) {
    await checkLoginSection();
    // @ts-ignore
    window.appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
        // size: 'invisible'
        size: 'normal'
    });
    // @ts-ignore
    const appVerifier = window.appVerifier;
    return firebaseAuth.signInWithPhoneNumber(phone, appVerifier).then(res => {
        return res;
    }).catch(err => {
        return err;
    });
}

// lấy FCM token
export async function getFcmToken() {
    try {
        await Notification.requestPermission();
        const message = firebase.messaging();

        const token = await message.getToken()
        return token;
    } catch (error) {
        console.log('error: ', error);
        return null;
    }
}
