import Cookies from 'js-cookie';

/**
 * Key of Cookies
 */
export enum CookieKey {
    TOKEN = 'WEB_TOKEN'
}

export class CookieUtils {
    /**
     * Lưu lại giá trị vào Cookies
     * @param key
     * @param value
     */
    static save(key: CookieKey, value: string, path?: string): void {
        Cookies.set(key, value, { path });
    }

    /**
     * Lấy giá trị từ trong Cookies
     * @param key
     */
    static get(key: CookieKey): string | undefined {
        return Cookies.get(key);
    }

    /**
     * Xóa giá trị đã lưu trong Cookies
     * @param key
     */
    static remove<T>(key: CookieKey, params?: T): void {
        Cookies.remove(key, params);
    }
}
