import firebase from 'firebase/app';
import "firebase/messaging";
import 'firebase/auth'
import { showAlertError } from './common.utils';

class FirebaseUtils {
    private auth: firebase.auth.Auth;
    public static instance: FirebaseUtils;

    constructor() {
        if (!firebase.apps.length) {
            firebase.initializeApp({
                apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
                authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
                projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
                messagingSenderId: process.env.NEXT_PUBLIC_FIREBASE_MESSAGE_SENDER_ID,
                appId: process.env.NEXT_PUBLIC_FIREBASE_APPID
            });
        }
        firebase.auth().useDeviceLanguage();
        this.auth = firebase.auth();
    }

    public static getInstance(): FirebaseUtils {
        if (!FirebaseUtils.instance) {
            FirebaseUtils.instance = new FirebaseUtils();
        }
        return FirebaseUtils.instance;
    }

    public async getFcmToken() {
        try {
            await Notification.requestPermission();
           
            const messaging = await firebase.messaging();
            const token = await messaging.getToken().then(token=> {
                console.log('token1: ', token);

            }).catch(error => {
                console.log('error: ', error);

            });
            console.log('token: ', token);
        } catch (error) {
            console.log('error: ', error);
        }
    }

    public async loginWithFacebook(): Promise<string | undefined> {
        const provider = new firebase.auth.FacebookAuthProvider();
        provider.setCustomParameters({
            display: 'popup'
        });

        return await this.signInWithPopup(provider);
    }

    public async loginWithGoogle(): Promise<string | undefined> {
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('https://www.googleapis.com/auth/userinfo.email');

        return await this.signInWithPopup(provider);
    }

    private async checkLoginSection() {
        const user = firebase.auth().currentUser;
        if (user) {
            await this.auth.signOut()
        }
    }

    public async sendOTP(phone: string) {
        await this.checkLoginSection();
        // @ts-ignore
        window.appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
            // size: 'invisible'
            size: 'normal'
        });
        // @ts-ignore
        const appVerifier = window.appVerifier;
        return this.auth.signInWithPhoneNumber(phone, appVerifier).then(res => {
            return res;
        }).catch(err => {
            return err;
        });
    }

    private async signInWithPopup(
        provider: firebase.auth.AuthProvider
    ): Promise<string | undefined> {
        return await this.auth
            .signInWithPopup(provider)
            .then((result: firebase.auth.UserCredential) => {
                const credential = result.credential;
                //@ts-ignore
                return credential!.accessToken;
            })
            .catch((error) => {
                showAlertError('loginFbFailure');
                return undefined;
            });
    };

}

export default FirebaseUtils;
