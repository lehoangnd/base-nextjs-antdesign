export const PasswordLength = {
    MIN: 6,
    MAX: 32
}

export const Limit = {
    DEFAULT: 9
}