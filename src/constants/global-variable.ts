import { User } from 'types/common.types';

export interface TokenInfor {
    token: string;
    refreshToken: string;
    expiresAt: string;
}

interface GlobalVariableParams {
    token?: TokenInfor;
    user?: User;
}

export const GlobalVariable: GlobalVariableParams = {};
