import { Media } from 'types/media.types';
import { ResBase } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';

export const IMAGE_UPLOAD = '/medias/upload-cloud';

export const onUploadImage = (file: any) => {
    return ApiUtils.postForm<any, ResBase<Media>>(IMAGE_UPLOAD, file);
};
