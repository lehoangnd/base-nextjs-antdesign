import MainLayout from '../libraries/layout/layout';
import HomeScreen from './home/home.screen';

export default function Home() {
    return (
        <MainLayout>
            <HomeScreen />
        </MainLayout>
    );
}
