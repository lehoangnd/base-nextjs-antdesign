import 'antd/dist/antd.css';
import appUtils from 'handler/app/app.utils';
import UserContext from 'libraries/context/user.context';
import App, { AppContext, AppProps } from 'next/app';
import React from 'react';
import { setI18nConfig } from 'res/languages';
import '../styles/becomeTeacher.scss';
import '../styles/effect.scss';
import '../styles/course.scss';
import '../styles/introduce.scss';
import '../styles/main.scss';
import '../styles/news.scss';
import '../styles/personal.scss';
import '../styles/lesson.scss';

setI18nConfig();


const MyApp = ({ Component, pageProps }: AppProps) => {
  const { user, totalOrder } = appUtils()

  return (
    <UserContext.Provider value={{ user, totalOrder }}>
      <Component {...pageProps} />
    </UserContext.Provider>
  );
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  const appProps = await App.getInitialProps(appContext);

  return { ...appProps }
}

export default MyApp;
