import homeUtils from 'handler/home/home.utils';
import React from 'react';
import VideoIntroComponent from './components/video-intro.component';

interface Props { }
export default function HomeScreen(props: Props) {
    const { banners } = homeUtils();
    return (
        <div>
            <VideoIntroComponent />
        </div>
    );
}
