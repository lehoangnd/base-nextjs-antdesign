import React from 'react';
import YouTube, { Options } from 'react-youtube';

const opts: Options = {
    height: '480',
    width: '100%',
    playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 1
    },
};

export default function VideoIntroComponent() {

    return (
        <div >
            <YouTube videoId="6t-MjBazs3o" opts={opts} />
        </div>
    )
}