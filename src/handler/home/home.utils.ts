import { useEffect, useState } from 'react';
import { fetchBanners } from './home.service';
import { Banner } from './home.types';

interface CateUtils {
    banners: Banner[];
}

export default function homeUtils(): CateUtils {
    const [banners, setBanners] = useState<Banner[]>([]);

    // lấy danh sách banner
    const fetchData = async (): Promise<void> => {
        const res = await fetchBanners();
        setBanners(res);
    };


    useEffect(() => {
        // didmount
        fetchData();
    }, []);

    return { banners };
}
