import { Media, ActiveType } from 'types/common.types';

export enum BannerType {
    DEFAULT = 'DEFAULT', // không điều hướng
    COURSE_DETAIL = 'COURSE_DETAIL', // điều hướng đến chi tiết khóa học
    NEWS_DETAIL = 'NEWS_DETAIL' // điều hướng đến chi tiết tin tức
}

export interface BannerPayload {
    courseId?: string;
    newsId?: string;
}

export interface Banner {
    _id: string;
    name: string;
    type: BannerType;
    payload?: BannerPayload;
    image?: Media;
}

