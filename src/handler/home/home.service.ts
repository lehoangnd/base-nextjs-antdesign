import { ResBase, ResCode } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';
import {
    Banner
} from './home.types';

const apiName = {
    Banner: 'banner',
};

export const fetchBanners = async (): Promise<Banner[]> => {
    const res = await ApiUtils.fetch<undefined, ResBase<Banner[]>>(
        apiName.Banner
    );
    let categories: Banner[] = [];
    if (res.status === ResCode.SUCCESS) {
        categories = res.data! || [];
    }
    return categories;
};

