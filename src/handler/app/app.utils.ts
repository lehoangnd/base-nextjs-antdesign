import { logout } from "handler/auth/auth.service";
import Router from "next/router";
import { useEffect, useRef, useState } from "react";
import { Subscription } from "rxjs";
import { User } from "types/common.types";
import { AsyncStorageUtils, StorageKey } from "utils/async-storage.utils";
import EventBus from "utils/event-bus/event-bus";
import { EventBusName } from "utils/event-bus/event-bus.types";
// import { getFcmToken } from "utils/firebase.utils";


interface UtilsRes {
    user: User | null;
    totalOrder: number;
}
export default function appUtils(): UtilsRes {
    const [user, setUser] = useState<User | null>(null);
    const [totalOrder, setTotalOrder] = useState<number>(0);
    const subscriptions = useRef<any>();

    useEffect(() => {
        fetchFcmToken();
        if (user) {
            fetchTotalOrder();
        }
    }, [user])

    useEffect(() => {
        // didmount
        initUser()

        registerEventBus();

        // unmout
        return () => {
            unregisterEventBus();
        };
    }, []);

    const fetchTotalOrder = async (): Promise<void> => {
        // const res = await apiTotalOrder()
        // setTotalOrder(res);
    };


    const initUser = (): void => {
        const user: User | null = AsyncStorageUtils.getObject(StorageKey.USER);
        setUser(user);
    };

    // subscribe notification
    const fetchFcmToken = async () => {
        let fcmTokenId = AsyncStorageUtils.get(StorageKey.FCM_TOKEN)
        if (!fcmTokenId) {
            // fcmTokenId = await getFcmToken();
            // AsyncStorageUtils.save(StorageKey.FCM_TOKEN, fcmTokenId!)
        }

        if (fcmTokenId && user) {
            // saveFcmToken({ fcmTokenId })
        }
    }

    // đăng xuất
    const onLogout = (): void => {
        setUser(null);
        setTotalOrder(0);
        logout();
        AsyncStorageUtils.remove(StorageKey.USER);
        AsyncStorageUtils.remove(StorageKey.TOKEN);
        Router.push('/');
    };

    // đăng ký eventbus
    const registerEventBus = (): void => {
        subscriptions.current = new Subscription();
        subscriptions.current.add(
            EventBus.getInstance().events.subscribe((data: any) => {
                switch (data.type) {
                    case EventBusName.AUTH_SUCCESS_EVENT:
                        initUser();
                        break;
                    case EventBusName.NO_TOKEN_EVENT:
                        setUser(null);
                        break;
                    case EventBusName.LOGOUT_EVENT:
                        onLogout();
                        break;
                    case EventBusName.FETCH_TOTAL_ORDER_EVENT:
                        fetchTotalOrder();
                        break;

                    default:
                        break;
                }
            })
        );
    };

    // hủy đăng ký eventBus
    const unregisterEventBus = (): void => {
        subscriptions.current?.unsubscribe();
    };

    return { user, totalOrder }
}