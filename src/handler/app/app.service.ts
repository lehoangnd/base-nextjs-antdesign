import { ResBase, ResCode } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';
import { FcmTokenInput } from './app.types';

const apiName = {
    subscribeNotification: 'notifications/subscribe'
};


export const saveFcmToken = async (
    params: FcmTokenInput
): Promise<boolean> => {
    const res = await ApiUtils.post<FcmTokenInput, ResBase<any>>(
        apiName.subscribeNotification,
        params
    );

    return !!res.data;
};