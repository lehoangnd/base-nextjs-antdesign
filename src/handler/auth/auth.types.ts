import { User } from 'types/common.types';
import { ResCode } from 'utils/api/api.types';

export interface RegisterUserInput {
    name: string;
    email: string;
    password: string;
    phone: string;
    dialCode: string;
}

export interface SigninInput {
    password: string;
    phone: string;
    dialCode: string;
}

export interface SigninEmailInput {
    password: string;
    email: string;
}

export interface AuthRes {
    user: User;
    token: string;
    refreshToken: string;
    expiresAt: string;
    status: ResCode;
}

export interface LogoutRes {
    status: ResCode;
}
export interface CheckPhoneRes {
    status: ResCode;
    verified: boolean;
    token?: string;
}

export enum CheckPhoneType {
    SIGNUP = 'SIGNUP',
    FORGOT_PASSWORD = 'FORGOT_PASSWORD'
}

export interface CheckUserByPhoneInput {
    dialCode: string;
    phone: string;
    type: CheckPhoneType;
    email?: string;
}

export interface ChangePasswordInput {
    token: string;
    password: string;
}

export enum SocialType {
    GOOGLE = 'GOOGLE',
    FACEBOOK = 'FACEBOOK'
}

export interface LoginSocialInput {
    type: SocialType;
    accessToken: string;
}

export interface CreateEmailOtpInput {
    email: string;
    code?: string;
}

export interface CheckEmail {
    email: string;
}

export interface EmailOtpRes {
    status: ResCode;
    data: boolean;
}
