import { GlobalVariable, TokenInfor } from 'constants/global-variable';
import { MessageAndStatusRes } from 'types/common.types';
import { ResCode, ResBase } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';
import { AsyncStorageUtils, StorageKey } from 'utils/async-storage.utils';
import {
    eventAuthSuccess,
    showAlertError,
    showAlertSuccess
} from 'utils/common.utils';
import {
    AuthRes,
    RegisterUserInput,
    LogoutRes,
    SigninInput,
    CheckUserByPhoneInput,
    CheckPhoneRes,
    ChangePasswordInput,
    LoginSocialInput,
    CreateEmailOtpInput,
    CheckEmail,
    EmailOtpRes,
    SigninEmailInput
} from './auth.types';

const apiName = {
    Signup: 'auth/signup',
    Logout: 'auth/logout',
    Signin: 'auth/signin',
    SigninEmail: 'auth/signin-email',
    CheckPhone: 'auth/check-phone',
    CheckEmail: 'auth/check-email',
    ChangePassword: 'auth/change-password',
    LoginSocial: 'auth/login-social',
    EmailOtp: 'email-otp',
    VerifyEmailOtp: 'email-otp/verify',
};

const handleAuthSuccess = (data: AuthRes): void => {
    const tokenInfor: TokenInfor = {
        token: data.token,
        refreshToken: data.refreshToken,
        expiresAt: data.expiresAt
    };
    AsyncStorageUtils.saveObject(StorageKey.TOKEN, tokenInfor);
    AsyncStorageUtils.saveObject(StorageKey.USER, data.user);
    GlobalVariable.token = tokenInfor;
    GlobalVariable.user = data.user;
    eventAuthSuccess();
};

export const signup = async (body: RegisterUserInput): Promise<boolean> => {
    const res = await ApiUtils.post<RegisterUserInput, AuthRes>(
        apiName.Signup,
        body
    );
    if (res.status === ResCode.SUCCESS) {
        handleAuthSuccess(res);
        showAlertSuccess('signupSuccess');
        return true;
    }
    let messageKey = 'signupFailure';
    switch (res.status) {
        case ResCode.USER_ALREADY_EXISTS:
            messageKey = 'userAlreadyExists';
            break;
        case ResCode.EMAIL_ALREADY_EXISTS:
            messageKey = 'emailAlreadyExists';
            break;
        case ResCode.PHONE_NUMBER_ALREADY_EXISTS:
            messageKey = 'phoneAlreadyExists';
            break;
        default:
            break;
    }
    showAlertError(messageKey);

    return false;
};

export const signin = async (body: SigninInput): Promise<boolean> => {
    const res = await ApiUtils.post<SigninInput, AuthRes>(apiName.Signin, body);
    if (res.status === ResCode.SUCCESS) {
        handleAuthSuccess(res);
        showAlertSuccess('signinSuccess');
        return true;
    }
    let messageKey = 'signinFailure';
    switch (res.status) {
        case ResCode.PHONE_CAN_NOT_BE_BLANK:
            messageKey = 'phoneRequired';
            break;
        case ResCode.PHONE_NUMBER_INVALID:
        case ResCode.DIAL_CODE_CAN_NOT_BE_BLANK:
            messageKey = 'phoneInvalid';
            break;
        case ResCode.USER_DO_NOT_EXISTS:
            messageKey = 'userDoNotExists';
            break;
        case ResCode.PASSWORD_INCORRECT:
            messageKey = 'passwordIncorrect';
            break;
        default:
            break;
    }
    showAlertError(messageKey);

    return false;
};

export const apiSigninEmail = async (body: SigninEmailInput): Promise<boolean> => {
    const res = await ApiUtils.post<SigninEmailInput, AuthRes>(apiName.SigninEmail, body);
    if (res.status === ResCode.SUCCESS) {
        handleAuthSuccess(res);
        showAlertSuccess('signinSuccess');
        return true;
    }
    let messageKey = 'signinFailure';
    switch (res.status) {
        case ResCode.EMAIL_CAN_NOT_BE_BLANK:
            messageKey = 'emailRequired';
            break;
        case ResCode.INVALID_EMAIL:
            messageKey = 'emailInvalid';
            break;
        case ResCode.USER_DO_NOT_EXISTS:
            messageKey = 'userDoNotExists';
            break;
        case ResCode.PASSWORD_INCORRECT:
            messageKey = 'passwordIncorrect';
            break;
        default:
            break;
    }
    showAlertError(messageKey);

    return false;
};

export const logout = async (): Promise<void> => {
    const res = await ApiUtils.post<undefined, LogoutRes>(apiName.Logout);
};

export const checkPhone = async (
    body: CheckUserByPhoneInput
): Promise<string | undefined> => {
    const res = await ApiUtils.post<CheckUserByPhoneInput, CheckPhoneRes>(
        apiName.CheckPhone,
        body
    );
    return res.token;
};

export const apiCheckEmail = async (
    body: CheckEmail
): Promise<string | undefined> => {
    const res = await ApiUtils.post<CheckEmail, CheckPhoneRes>(
        apiName.CheckEmail,
        body
    );
    return res.token;
};

export const checkEmailPhone = async (
    body: CheckUserByPhoneInput
): Promise<MessageAndStatusRes> => {
    const res = await ApiUtils.post<CheckUserByPhoneInput, CheckPhoneRes>(
        apiName.CheckPhone,
        body
    );
    let messageKey = 'phoneAlreadyExists';
    if (res.status === ResCode.EMAIL_ALREADY_EXISTS) {
        messageKey = 'emailAlreadyExists'
    }

    return {
        status: res.verified,
        messageKey
    };
};

export const changePassword = async (
    body: ChangePasswordInput
): Promise<boolean | undefined> => {
    try {
        const res = await ApiUtils.post<ChangePasswordInput, ResBase<boolean>>(
            apiName.ChangePassword,
            body
        );
        return res.data;
    } catch (error) {
        return;
    }
};

export const loginSocial = async (
    body: LoginSocialInput
): Promise<boolean | undefined> => {
    try {
        const res = await ApiUtils.post<LoginSocialInput, AuthRes>(
            apiName.LoginSocial,
            body
        );
        if (res.status === ResCode.SUCCESS) {
            handleAuthSuccess(res);
            showAlertSuccess('signinSuccess');
            return true;
        }
        return false;
    } catch (error) {
        return false;
    }
};

export const apiSendOtp = async ({ email }: CreateEmailOtpInput) => {
    try {
        const res = await ApiUtils.post<CreateEmailOtpInput, EmailOtpRes>(
            apiName.EmailOtp,
            { email }
        );
       
        return res.status === ResCode.SUCCESS;
    } catch (error) {
        return false;
    }
}

export const apiVerifyOtp = async ({ email, code }: CreateEmailOtpInput): Promise<boolean> => {
    try {
        const res = await ApiUtils.post<CreateEmailOtpInput, EmailOtpRes>(
            apiName.VerifyEmailOtp,
            { email, code }
        );
       
        return res.data;
    } catch (error) {
        return false;
    }
}
