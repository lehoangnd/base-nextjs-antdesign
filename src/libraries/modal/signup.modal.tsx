import { CloseCircleOutlined } from '@ant-design/icons';
import { Form, Modal } from 'antd';
import { checkEmailPhone } from 'handler/auth/auth.service';
import { CheckPhoneType } from 'handler/auth/auth.types';
import React, { useEffect, useRef, useState } from 'react';
import { translates } from 'res/languages';
import { Subscription } from 'rxjs';
import {
    showAlertError,
    showModalOtp,
    showModalSignin
} from 'utils/common.utils';
import EventBus from 'utils/event-bus/event-bus';
import { EventBusName } from 'utils/event-bus/event-bus.types';
import { TextUtils } from 'utils/text.utils';
import ButtonComponent from './components/button.component';
import InputComponent from './components/input.component';
import LineComponent from './components/line.component';
import PhoneComponent from './components/phone.component';
import SocialButtonComponent from './components/social-button.component';
import styles from './styles.module.scss';

interface Props {}

interface FormValues {
    name: string;
    phone: string;
    email: string;
    password: string;
}

function TitleModal() {
    return <div className={styles.modalTitle}>{translates('signup')}</div>;
}

export default function SignupModal(props: Props) {
    const [visible, setVisible] = useState<boolean>(false);
    const subscriptions = useRef<any>();
    const [form] = Form.useForm<FormValues>();
    const [dialCode, setDialCode] = useState<string>('+84');

    useEffect(() => {
        registerEventBus();

        return () => {
            unregisterEventBus();
        };
    }, []);

    // đăng ký eventbus
    const registerEventBus = (): void => {
        subscriptions.current = new Subscription();
        subscriptions.current.add(
            EventBus.getInstance().events.subscribe((data: any) => {
                switch (data.type) {
                    case EventBusName.SHOW_SIGNUP_EVENT:
                        setVisible(true);
                        break;

                    default:
                        break;
                }
            })
        );
    };

    // hủy đăng ký eventBus
    const unregisterEventBus = (): void => {
        subscriptions.current?.unsubscribe();
    };

    // đóng modal
    const onCloseModal = (): void => {
        setVisible(false);
    };

    // hiển thị đăng nhập
    const onShowLogin = (): void => {
        setVisible(false);
        showModalSignin();
    };

    // xử lý đăng ký
    const handleSubmit = async (values: FormValues): Promise<void> => {
        const phone = TextUtils.removeDialCodePhone(values.phone, dialCode);
        const { status, messageKey } = await checkEmailPhone({
            phone,
            dialCode,
            email: values.email,
            type: CheckPhoneType.SIGNUP
        });
        if (status) {
            setVisible(false);
            showModalOtp({
                phone,
                dialCode,
                email: values.email,
                password: values.password,
                name: values.name
            });
        } else {
            showAlertError(messageKey);
        }
    };

    // thay đổi số điện thoại
    const onChangePhone = (value: string, data: any): void => {
        setDialCode(`+${data.dialCode}`);
    };

    return (
        <Modal
            className="signupModal"
            onCancel={onCloseModal}
            visible={visible}
            footer={null}
            closeIcon={<CloseCircleOutlined className={styles.icClose} />}
            title={<TitleModal />}
        >
            <Form form={form} onFinish={handleSubmit}>
                <InputComponent
                    name="name"
                    placeholder="fullname"
                    errorText="fullnameRequired"
                    errorTextLength="fullnameLengthInvalid"
                />
                <PhoneComponent dialCode={dialCode} onChange={onChangePhone} />
                <InputComponent
                    name="email"
                    placeholder="email"
                    errorText="emailRequired"
                    errorTextLength="emailLengthInvalid"
                    isEmail
                />
                <InputComponent
                    name="password"
                    placeholder="password"
                    errorText="passwordRequired"
                    errorTextLength="passwordLengthInvalid"
                    minLength={6}
                    isPassword
                />
                <ButtonComponent />
                <LineComponent />
                <SocialButtonComponent isSignup onCloseModal={onCloseModal} />
                <div className={styles.hack} />
                <div className={styles.youAreMember}>
                    <p className={styles.youAreMemberTxt}>
                        {translates('youAreMember')}
                    </p>
                    <p
                        onClick={onShowLogin}
                        className={styles.youAreMemberTxt2}
                    >
                        {translates('signin')}
                    </p>
                </div>
            </Form>
        </Modal>
    );
}
