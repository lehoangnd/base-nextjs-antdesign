import { Button } from 'antd';
import { loginSocial } from 'handler/auth/auth.service';
import { SocialType } from 'handler/auth/auth.types';
import { translates } from 'res/languages';
import { showAlertError, showLoading } from 'utils/common.utils';
// import { loginWithFacebook, loginWithGoogle }  from 'utils/firebase.utils';
import styles from '../styles.module.scss';

interface Props {
    isSignup?: boolean;
    onCloseModal: () => void;
}

export default function SocialButtonComponent(props: Props) {
    // đăng nhập bằng facebook
    const onLoginFacebook = async (): Promise<void> => {
        // const accessToken = await loginWithFacebook();
        const accessToken = '';
        if (!accessToken) {
            showAlertError(
                props.isSignup ? 'signupFbFailure' : 'loginFbFailure'
            );
        } else {
            showLoading(props.isSignup ? 'loadingSignup' : 'loadingSignin');
            const res = await loginSocial({
                accessToken,
                type: SocialType.FACEBOOK
            });
            if (!res) {
                showAlertError(
                    props.isSignup ? 'signupGgFailure' : 'loginGgFailure'
                );
            } else {
                props.onCloseModal();
            }
        }
    };
    // đăng nhập bằng google
    const onLoginGoogle = async (): Promise<void> => {
        // const accessToken = await loginWithGoogle();
        const accessToken = '';
        if (!accessToken) {
            showAlertError(
                props.isSignup ? 'signupGgFailure' : 'loginGgFailure'
            );
        } else {
            showLoading(props.isSignup ? 'loadingSignup' : 'loadingSignin');
            const res = await loginSocial({
                accessToken,
                type: SocialType.GOOGLE
            });
            if (!res) {
                showAlertError(
                    props.isSignup ? 'signupGgFailure' : 'loginGgFailure'
                );
            } else {
                props.onCloseModal();
            }
        }
    };

    return (
        <div className={styles.socialView}>
            <Button
                type="primary"
                size="large"
                className={styles.btnGg}
                onClick={onLoginGoogle}
            >
                <img src="/auth/icGg.png" alt="Amostudy icon" height={15.74} />
                <p className={styles.txt}>
                    {translates(props.isSignup ? 'signupGg' : 'loginGg')}
                </p>
            </Button>
            <Button
                type="primary"
                size="large"
                className={styles.btnFb}
                onClick={onLoginFacebook}
            >
                <img src="/auth/icFb.png" alt="Amostudy icon" height={21} />
                <p className={styles.txt}>
                    {translates(props.isSignup ? 'signupFb' : 'loginFb')}
                </p>
            </Button>
        </div>
    );
}
