import { translates } from "res/languages";
import styles from '../styles.module.scss'


export default function LineComponent() {
    return (
        <div className={styles.lineView}>
            <div className={styles.line} />
            <p className={styles.lineText}>{translates('or')}</p>
            <div className={styles.line} />
        </div>
    );
}
