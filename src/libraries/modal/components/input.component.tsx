import React from 'react';
import { Form, Input } from 'antd';
import { translates } from 'res/languages';
import styles from '../styles.module.scss';

interface Props {
    name: string;
    placeholder: string;
    errorText: string;
    errorTextLength: string;
    minLength?: number;
    maxLength?: number;
    color?: string;
    isPassword?: boolean;
    isEmail?: boolean;
}

export default function InputComponent(props: Props) {
    return (
        <Form.Item
            name={props.name}
            labelCol={{ span: 7 }}
            labelAlign="left"
            rules={[
                {
                    whitespace: true,
                    required: true,
                    message: translates(props.errorText)
                },
                props.isEmail
                    ? {
                          pattern: /^[\w-\\.]+@([\w-]+\.)+[\w-]+$/,
                          message: translates(props.errorTextLength)
                      }
                    : {
                          transform: (value) => {
                              return value?.trim();
                          },
                          min: props.minLength || 2,
                          max: props.maxLength || 200,
                          message: translates(props.errorTextLength)
                      }
            ]}
        >
            {props.isPassword ? (
                <Input.Password
                    placeholder={translates(props.placeholder)}
                    color={props.color || '#030504'}
                    className={styles.input}
                    size="large"
                />
            ) : (
                <Input
                    className={styles.input}
                    size="large"
                    placeholder={translates(props.placeholder)}
                    color={props.color || '#030504'}
                />
            )}
        </Form.Item>
    );
}
