import { Form } from 'antd';
import { translates } from 'res/languages';
import { TextUtils } from 'utils/text.utils';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/plain.css';
import { RegexUtils } from 'utils/regex.utils';
import styles from '../styles.module.scss';

interface Props {
    dialCode: string;
    onChange: (phone: string, data: any) => void;
}

export default function PhoneComponent(props: Props) {
    return (
        <Form.Item
            rules={[
                () => ({
                    validator(rule, value) {
                        if (!value) {
                            return Promise.reject(
                                new Error(translates('phoneRequired'))
                            );
                        }
                        const phone = TextUtils.removeDialCodePhone(
                            value,
                            props.dialCode
                        );

                        const checkPhone = RegexUtils.isVietnamesePhoneStaff(
                            phone
                        );
                        if (!checkPhone) {
                            return Promise.reject(
                                new Error(translates('phoneInvalid'))
                            );
                        }
                        return Promise.resolve();
                    }
                })
            ]}
            name="phone"
        >
            <div className="inputPhone">
                <PhoneInput
                    country={'vn'}
                    onChange={props.onChange}
                    inputClass={styles.input_phone}
                    enableSearch={false}
                    autocompleteSearch={false}
                    countryCodeEditable={false}
                    searchStyle={{
                        outline: 'none'
                    }}
                    disableSearchIcon={true}
                    inputProps={{
                        required: true,
                        autoFocus: true
                    }}
                />
            </div>
        </Form.Item>
    );
}
