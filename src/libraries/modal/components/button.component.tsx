import { Button } from "antd";
import { translates } from "res/languages";
import { Form } from 'antd';

interface Props {
    title?: string;
    loading?: boolean;
}

export default function ButtonComponent(props: Props) {
    return (
        <Form.Item>
            <Button
                loading={props.loading}
                size="large"
                type="primary"
                htmlType="submit"
                style={{
                    background: '#F492B3',
                    borderRadius: '3px',
                    border: 'none',
                    width: '100%',
                    fontSize: 18,
                    height: 42
                }}
            >
                {translates(props.title || 'signup')}
            </Button>
        </Form.Item>
    );
}
