import { CloseCircleOutlined } from '@ant-design/icons';
import { Form, Modal } from 'antd';
import { apiSigninEmail } from 'handler/auth/auth.service';
import React, { useEffect, useRef, useState } from 'react';
import { translates } from 'res/languages';
import { Subscription } from 'rxjs';
import {
    showLoading,
    showModalForgotPassword,
    showModalSignup
} from 'utils/common.utils';
import EventBus from 'utils/event-bus/event-bus';
import { EventBusName } from 'utils/event-bus/event-bus.types';
import ButtonComponent from './components/button.component';
import InputComponent from './components/input.component';
import LineComponent from './components/line.component';
import SocialButtonComponent from './components/social-button.component';
import styles from './styles.module.scss';

interface Props { }

interface FormValues {
    name: string;
    phone: string;
    email: string;
    password: string;
}

function TitleModal() {
    return <div className={styles.modalTitle}>{translates('signin')}</div>;
}

export default function SigninModal(props: Props) {
    const [visible, setVisible] = useState<boolean>(false);
    const subscriptions = useRef<any>();
    const [form] = Form.useForm<FormValues>();
    const [dialCode, setDialCode] = useState<string>('+84');

    useEffect(() => {
        registerEventBus();

        return () => {
            unregisterEventBus();
        };
    }, []);

    // đăng ký eventbus
    const registerEventBus = (): void => {
        subscriptions.current = new Subscription();
        subscriptions.current.add(
            EventBus.getInstance().events.subscribe((data: any) => {
                switch (data.type) {
                    case EventBusName.SHOW_SIGNIN_EVENT:
                        setVisible(true);
                        break;

                    default:
                        break;
                }
            })
        );
    };

    // hủy đăng ký eventBus
    const unregisterEventBus = (): void => {
        subscriptions.current?.unsubscribe();
    };

    // đóng modal
    const onCloseModal = (): void => {
        setVisible(false);
    };

    // xử lý đăng nhập
    const handleSubmit = async (values: FormValues): Promise<void> => {
        showLoading('loadingSignin');
        const res = await apiSigninEmail({
            password: values.password,
            email: values.email.trim()
        });
        if (res) {
            setVisible(false);
        }
    };

    // thay đổi số điện thoại
    const onChangePhone = (value: string, data: any): void => {
        setDialCode(`+${data.dialCode}`);
    };

    // hiển thị đăng ký
    const onShowLogin = (): void => {
        setVisible(false);
        showModalSignup();
    };

    // hiển thị quên mật khẩu
    const onShowForgot = (): void => {
        setVisible(false);
        showModalForgotPassword();
    };

    return (
        <Modal
            className="signupModal"
            onCancel={onCloseModal}
            visible={visible}
            footer={null}
            closeIcon={<CloseCircleOutlined className={styles.icClose} />}
            title={<TitleModal />}
        >
            <Form form={form} onFinish={handleSubmit}>
                <SocialButtonComponent onCloseModal={onCloseModal} />
                <LineComponent />
                <label htmlFor="email" className={styles.label}>
                    {translates('email')}
                </label>
                {/* <PhoneComponent dialCode={dialCode} onChange={onChangePhone} /> */}
                <InputComponent
                    name="email"
                    placeholder="email"
                    errorText="emailRequired"
                    errorTextLength="emailLengthInvalid"
                    isEmail
                />
                <label htmlFor="password" className={styles.label}>
                    {translates('password')}
                </label>
                <InputComponent
                    name="password"
                    placeholder="password"
                    errorText="passwordRequired"
                    errorTextLength="passwordLengthInvalid"
                    minLength={6}
                    isPassword
                />
                <ButtonComponent title="signin" />
                <div className={styles.notMemberView}>
                    <p
                        onClick={onShowForgot}
                        className={styles.youAreMemberTxt2}
                    >
                        {translates('forgotPass')}
                    </p>
                    <div className={styles.youAreMember}>
                        <p className={styles.youAreMemberTxt}>
                            {translates('notMember')}
                        </p>
                        <p
                            onClick={onShowLogin}
                            className={styles.youAreMemberTxt2}
                        >
                            {translates('signup')}
                        </p>
                    </div>
                </div>
            </Form>
        </Modal>
    );
}
