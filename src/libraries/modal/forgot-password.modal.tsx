import { CloseCircleOutlined } from '@ant-design/icons';
import { Form, Modal } from 'antd';
import { apiCheckEmail } from 'handler/auth/auth.service';
import React, { useEffect, useRef, useState } from 'react';
import { translates } from 'res/languages';
import { Subscription } from 'rxjs';
import {
    showAlertError,
    showLoading,
    showModalOtp,
    showModalSignin,
    showModalSignup
} from 'utils/common.utils';
import EventBus from 'utils/event-bus/event-bus';
import { EventBusName } from 'utils/event-bus/event-bus.types';
import ButtonComponent from './components/button.component';
import InputComponent from './components/input.component';
import styles from './styles.module.scss';

interface Props { }

interface FormValues {
    email: string;
}

function TitleModal() {
    return (
        <div className={styles.modalTitle}>
            {translates('getPassword.title')}
        </div>
    );
}

export default function ForgotPasswordModal(props: Props) {
    const [visible, setVisible] = useState<boolean>(false);
    const subscriptions = useRef<any>();
    const [form] = Form.useForm<FormValues>();
    const [dialCode, setDialCode] = useState<string>('+84');

    useEffect(() => {
        registerEventBus();

        return () => {
            unregisterEventBus();
        };
    }, []);

    // đăng ký eventbus
    const registerEventBus = (): void => {
        subscriptions.current = new Subscription();
        subscriptions.current.add(
            EventBus.getInstance().events.subscribe((data: any) => {
                switch (data.type) {
                    case EventBusName.SHOW_FORGOT_PASSWORD_EVENT:
                        setVisible(true);
                        break;

                    default:
                        break;
                }
            })
        );
    };

    // hủy đăng ký eventBus
    const unregisterEventBus = (): void => {
        subscriptions.current?.unsubscribe();
    };

    // đóng modal
    const onCloseModal = (): void => {
        setVisible(false);
    };

    // xử lý đăng ký
    const handleSubmit = async ({ email }: FormValues): Promise<void> => {
        showLoading('getPassword.loadingForgot');
        const token = await apiCheckEmail({ email });
        if (!token) {
            showAlertError('getPassword.notMember');
        } else {
            setVisible(false);
            showModalOtp({ email, token });
        }
    };

    // thay đổi số điện thoại
    const onChangePhone = (value: string, data: any): void => {
        setDialCode(`+${data.dialCode}`);
    };

    // hiển thị đăng ký
    const onShowLogin = (): void => {
        setVisible(false);
        showModalSignin();
    };

    // hiển thị quên mật khẩu
    const onShowSignup = (): void => {
        setVisible(false);
        showModalSignup();
    };

    return (
        <Modal
            className="signupModal"
            onCancel={onCloseModal}
            visible={visible}
            footer={null}
            closeIcon={<CloseCircleOutlined className={styles.icClose} />}
            title={<TitleModal />}
        >
            <Form form={form} onFinish={handleSubmit}>
                <p className={styles.labelForgot}>
                    {translates('getPassword.desc')}
                </p>
                <InputComponent
                    name="email"
                    placeholder="email"
                    errorText="emailRequired"
                    errorTextLength="emailLengthInvalid"
                    isEmail
                />
                <ButtonComponent title="getPassword.send" />
                <div className={styles.notMemberView}>
                    <p
                        onClick={onShowLogin}
                        className={styles.youAreMemberTxt2}
                    >
                        {translates('signin')}
                    </p>
                    <div className={styles.youAreMember}>
                        <p className={styles.youAreMemberTxt}>
                            {translates('notMember')}
                        </p>
                        <p
                            onClick={onShowSignup}
                            className={styles.youAreMemberTxt2}
                        >
                            {translates('signup')}
                        </p>
                    </div>
                </div>
            </Form>
        </Modal>
    );
}
