import { CloseCircleOutlined } from '@ant-design/icons';
import { Form, Modal } from 'antd';
import { apiSendOtp, apiVerifyOtp, signup } from 'handler/auth/auth.service';
import { RegisterUserInput } from 'handler/auth/auth.types';
import React, { useEffect, useRef, useState } from 'react';
import OtpInput from 'react-otp-input';
import { translates } from 'res/languages';
import { Subscription } from 'rxjs';
import {
    showAlertError,
    showLoading,
    showModalChangePassword
} from 'utils/common.utils';
import EventBus from 'utils/event-bus/event-bus';
import { EventBusName } from 'utils/event-bus/event-bus.types';
import ButtonComponent from './components/button.component';
import styles from './styles.module.scss';

interface Props {}

interface FormValues {
    name: string;
    phone: string;
    email: string;
    password: string;
}

function TitleModal() {
    return (
        <div className={styles.modalTitle}>{translates('confirm.title')}</div>
    );
}

export default function OtpModal(props: Props) {
    const [visible, setVisible] = useState<boolean>(false);
    const subscriptions = useRef<any>();
    const [form] = Form.useForm<FormValues>();
    const [email, setEmail] = useState<string>('');
    const [token, setToken] = useState<string>('');
    const [opt, setOtp] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [signupData, setSignupData] = useState<RegisterUserInput>();
    const confirmationResult = useRef<any>();

    useEffect(() => {
        registerEventBus();

        return () => {
            unregisterEventBus();
        };
    }, []);
    
    useEffect(() => {
        if (email !== '') {
            sendOTPToUser();
        }
    }, [email]);

    // gửi mã xác minh
    const sendOTPToUser = async () => {
        setLoading(true);
        const res = await apiSendOtp({ email });
        setLoading(false);
    };

    
    const verifyOTP = async (): Promise<boolean> => {
        setLoading(true);
        const res = await apiVerifyOtp({ email, code: opt });
        setLoading(false);
        return res;
    };



    // đăng ký eventbus
    const registerEventBus = (): void => {
        subscriptions.current = new Subscription();
        subscriptions.current.add(
            EventBus.getInstance().events.subscribe((data: any) => {
                switch (data.type) {
                    case EventBusName.SHOW_OTP_EVENT:
                        if (!data.payload) {
                            return;
                        }
                        const payload = data.payload;
                        setVisible(true);
                        if (payload.token) {
                            setEmail(data.payload.email);
                            setToken(data.payload.token);
                        } else {
                            setSignupData({
                                phone: payload.phone,
                                email: payload.email,
                                password: payload.password,
                                name: payload.name,
                                dialCode: payload.dialCode
                            });

                            setEmail(data.payload.email);
                        }
                        break;

                    default:
                        break;
                }
            })
        );
    };

    // hủy đăng ký eventBus
    const unregisterEventBus = (): void => {
        subscriptions.current?.unsubscribe();
    };

    // đóng modal
    const onCloseModal = (): void => {
        setVisible(false);
    };

    const handleError = (): void => {
        setLoading(false);
        showAlertError('confirm.otpInvalid');
        setOtp('');
    };

    const handleSignup = async (): Promise<void> => {
        showLoading('loadingSignup');
        const res = await signup(signupData!);
        if (res) {
            setVisible(false);
        }
    };

    // Xác minh otp
    const handleSubmit = async (values: FormValues): Promise<void> => {
        setLoading(true);
        try {
            const res = await verifyOTP();
            if (res) {
                setVisible(false);
                if (signupData) {
                    handleSignup();
                } else {
                    showModalChangePassword({ token });
                }
            } else {
                handleError();
            }
        } catch (error) {
            handleError();
        }
    };

    // Gửi lại mã
    const onResendOtp = (): void => {
        sendOTPToUser();
    };

    if (!visible) return null;

    return (
        <Modal
            className="signupModal"
            onCancel={onCloseModal}
            visible={visible}
            footer={null}
            closeIcon={<CloseCircleOutlined className={styles.icClose} />}
            title={<TitleModal />}
        >
            <Form form={form} onFinish={handleSubmit}>
                <p className={styles.labelForgot}>
                    {translates('confirm.desc', { email })}
                </p>
                <Form.Item>
                    <OtpInput
                        value={opt}
                        onChange={setOtp}
                        numInputs={6}
                        containerStyle={styles.wrapper_input_otp}
                        inputStyle={styles.input_otp}
                        isInputNum={true}
                    />
                </Form.Item>
                <ButtonComponent title="continue" loading={loading} />
                <div className={styles.hack} />
                <div className={styles.youAreMember}>
                    <p className={styles.youAreMemberTxt}>
                        {translates('confirm.resendText')}
                    </p>
                    <p
                        onClick={onResendOtp}
                        className={styles.youAreMemberTxt2}
                    >
                        {translates('confirm.resend')}
                    </p>
                </div>

                <div id="recaptcha-container" style={{ display: 'none' }} />
            </Form>
        </Modal>
    );
}
