import React, { useEffect, useRef, useState } from 'react';
import { Modal, Form } from 'antd';
import { EventBusName } from 'utils/event-bus/event-bus.types';
import EventBus from 'utils/event-bus/event-bus';
import { Subscription } from 'rxjs';
import { CloseCircleOutlined } from '@ant-design/icons';
import styles from './styles.module.scss';
import { translates } from 'res/languages';
import InputComponent from './components/input.component';
import PhoneComponent from './components/phone.component';
import ButtonComponent from './components/button.component';
import { TextUtils } from 'utils/text.utils';
import { showAlertError, showAlertSuccess, showLoading, showModalForgotPassword, showModalSignup } from 'utils/common.utils';
import { changePassword, signin, signup } from 'handler/auth/auth.service';
import LineComponent from './components/line.component';
import SocialButtonComponent from './components/social-button.component';

interface Props {}

interface FormValues {
    password: string;
    rePassword: string;
}

function TitleModal() {
    return <div className={styles.modalTitle}>{translates('changePass.title')}</div>;
}

export default function ChangePasswordModal(props: Props) {
    const [visible, setVisible] = useState<boolean>(false);
    const subscriptions = useRef<any>();
    const [form] = Form.useForm<FormValues>();
    const [token, setToken] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        registerEventBus();

        return () => {
            unregisterEventBus();
        };
    }, []);

    // đăng ký eventbus
    const registerEventBus = (): void => {
        subscriptions.current = new Subscription();
        subscriptions.current.add(
            EventBus.getInstance().events.subscribe((data: any) => {
                switch (data.type) {
                    case EventBusName.SHOW_CHANGE_PASSWORD_EVENT:
                        setVisible(true);
                        setToken(data.payload.token);
                        break;

                    default:
                        break;
                }
            })
        );
    };

    // hủy đăng ký eventBus
    const unregisterEventBus = (): void => {
        subscriptions.current?.unsubscribe();
    };

    // đóng modal
    const onCloseModal = (): void => {
        setVisible(false);
    };

    // xử lý đổi mật khẩu
    const handleSubmit = async (values: FormValues): Promise<void> => {
        if (values.password !== values.rePassword) {
            showAlertError('changePass.repassInvalid');
            return;
        }
        setLoading(true)
        const res = await changePassword({
            password: values.password,
            token: `Bearer ${token}`
        });
        
        if (res) {
            showAlertSuccess('changePass.success')
            setVisible(false);
        } else {
            showAlertError('changePass.error')
            setLoading(false)
        }
    };

    return (
        <Modal
            className="signupModal"
            onCancel={onCloseModal}
            visible={visible}
            footer={null}
            closeIcon={<CloseCircleOutlined className={styles.icClose} />}
            title={<TitleModal />}
        >
            <Form form={form} onFinish={handleSubmit}>
                <InputComponent
                    name="password"
                    placeholder="password"
                    errorText="passwordRequired"
                    errorTextLength="passwordLengthInvalid"
                    minLength={6}
                    isPassword
                />
                
                <InputComponent
                    name="rePassword"
                    placeholder="changePass.rePassword"
                    errorText="rePasswordRequired"
                    errorTextLength="rePasswordLengthInvalid"
                    minLength={6}
                    isPassword
                />
                <ButtonComponent title="changePass.done" loading={loading} />
            </Form>
        </Modal>
    );
}
