import React from 'react';
import { translates } from 'res/languages';
import { Breadcrumb } from 'antd';
import { RightOutlined } from '@ant-design/icons'

interface Props {
    title: string;
    path: string;
    detail?: string;
    className?: string;
}

function PageTitle(props: Props) {
    return (
        // <div className={`pageTitleView ${props.className || ''}`}>
        //     <a href="/">
        //         <p className="pageTitle">{translates('home.home')}</p>{' '}
        //     </a>

        //     <img src="/news/arrowRight.png" alt="" className="ptImage" />
        //     <a href={props.path}>
        //         <p className={'pageTitleAt'}>{translates(props.title)}</p>
        //     </a>

        //     {props.detail && (
        //         <img src="/news/arrowRight.png" alt="" className="ptImage" />
        //     )}
        //     {props.detail && <p className="pageTitle">{props.detail}</p>}
        // </div>
        <Breadcrumb separator={<RightOutlined />} className="pageTitleView">
            <Breadcrumb.Item href="/">{translates('home.home')}</Breadcrumb.Item>
            <Breadcrumb.Item href={props.path}>{translates(props.title)}</Breadcrumb.Item>
            {props.detail && <Breadcrumb.Item className="pageTitle">{props.detail}</Breadcrumb.Item>}
        </Breadcrumb>
    );
}

export default PageTitle;
