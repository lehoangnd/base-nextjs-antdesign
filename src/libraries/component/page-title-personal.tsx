import React from 'react';
import { translates } from 'res/languages';
import { Breadcrumb } from 'antd';
import { RightOutlined } from '@ant-design/icons'
import { useRouter } from 'next/router';

interface Props {
    titleMain: string;
    title: string;
    typePage: string;
    activeType:(type: string) => void;
}

function PageTitlePersonal(props: Props) {
    const router = useRouter();

    const backToPage = () => {
        router.push('/personal');
        props.activeType(props.typePage)
    };

    return (
        <Breadcrumb separator={<RightOutlined />} className="pageTitleViewPersonal">
            <Breadcrumb.Item onClick={backToPage}>{translates(props.titleMain)}</Breadcrumb.Item>
            <Breadcrumb.Item className="pageTitle">{translates(props.title)}</Breadcrumb.Item>
        </Breadcrumb>
    );
}

export default PageTitlePersonal;
