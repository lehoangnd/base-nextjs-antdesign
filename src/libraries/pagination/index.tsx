import { Col, Pagination } from 'antd';
import { Limit } from 'constants/constants';
import React from 'react';
import styles from './styles.module.scss'

interface Props {
    page: number;
    total: number;
    onPageChange: (page: number) => void;
}

export default class BasePagination extends React.PureComponent<Props> {
    render() {
        const { page, total, onPageChange } = this.props;
        return (
            <Col
                span={24}
                className={`${styles.paginate} main-content`}
            >
                <div className={`d-flex justify-content-end paging-custom ${styles.pagination}`}>
                    <Pagination
                        className="ok"
                        size="default"
                        showSizeChanger={false}
                        total={total}
                        // showTotal={(total, range) =>
                        //     `${range[0]}-${range[1]}/${total}`
                        // }
                        current={page}
                        pageSize={Limit.DEFAULT}
                        defaultPageSize={Limit.DEFAULT}
                        onChange={onPageChange}
                    />
                </div>
            </Col>
        );
    }
}
