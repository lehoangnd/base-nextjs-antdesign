import { createContext } from 'react';
import { User } from 'types/common.types';

interface MyContext {
    user: User | null;
    totalOrder: number;
}

const initContext: MyContext = {
    user: null,
    totalOrder: 0
}

const UserContext = createContext(initContext);

export default UserContext;
