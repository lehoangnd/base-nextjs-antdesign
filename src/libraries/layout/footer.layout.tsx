import { Layout } from 'antd';
import FooterComponent from './components/footer.component';
// @ts-ignore
import styles from './layout.module.scss';

const { Footer } = Layout;

interface Props {}

function FooterLayout(props: Props) {
    return (
        <>
            <FooterComponent />
            <Footer className={styles.footer}>
                Copyright &copy; 2021 Amostudy - Created by{' '}
                <a href="http://leatech.vn" target="_blank">
                    leatech.vn
                </a>{' '}
                team
            </Footer>
        </>
    );
}

export default FooterLayout;
