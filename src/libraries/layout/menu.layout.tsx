import { SearchOutlined } from '@ant-design/icons';
import { Button, Col, Input, Layout, Row } from 'antd';
import { translates } from 'res/languages';
// @ts-ignore
import styles from './layout.module.scss';

const { Header } = Layout;

interface Props {}

function MenuLayout(props: Props) {
    return (
        <Header className={styles.menuWrapper}>
            <Row gutter={24}>
                <Col span={6}>
                    <a href="/" className={styles.menuLogoItem}>
                        <img
                            src="./logo/icLogo.png"
                            alt="Amostudy Logo"
                            height="80"
                        />
                    </a>
                </Col>
                <Col span={18} className={styles.menuRight}>
                    <Button type="link" className={styles.menuItem}>
                        {translates('course')}
                    </Button>
                    <Button type="link" className={styles.menuItem}>
                        {translates('introduce')}
                    </Button>
                    <Button type="link" className={styles.menuItem}>
                        {translates('document')}
                    </Button>
                    <Button type="link" className={styles.menuItem}>
                        {translates('news')}
                    </Button>
                    <Button type="link" className={styles.menuItem}>
                        {translates('becomeATeacher')}
                    </Button>

                    <Button type="link" className={styles.menuItem}>
                        {translates('contact')}
                    </Button>
                    <Input
                        size="large"
                        placeholder={translates('search')}
                        prefix={
                            <SearchOutlined className={styles.iconSearch} />
                        }
                        className={styles.menuInput}
                    />
                    <Button type="link" className={styles.icCart}>
                        <img src="/menu/icCart.png" width={26} />
                    </Button>
                    <Button type="link" className={styles.icYoutube}>
                        <img src="/menu/icUser.png" width={30} />
                    </Button>
                </Col>
            </Row>
        </Header>
    );
}

export default MenuLayout;
