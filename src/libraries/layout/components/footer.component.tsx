import { Button, Col, Form, Input, Row, Select } from 'antd';
import React from 'react';
import { FiPhoneCall } from 'react-icons/fi';
import { GoLocation } from 'react-icons/go';
import {
    TiSocialFacebook,

    TiSocialTwitter, TiSocialYoutube
} from 'react-icons/ti';
import { translates } from 'res/languages';
import { showLoading } from 'utils/common.utils';
import { RegexUtils } from 'utils/regex.utils';

interface Props { }

interface FormRes {
    phone: string;
    name: string;
    email: string;
    branchId: string;
}

const { Item, useForm } = Form;
const { Option } = Select;

export default function FooterComponent(props: Props) {
    const [form] = useForm<FormRes>()


    const toCapitalize = (str: string) => {
        return str
            .toLowerCase()
            .split(' ')
            .map(function (Word) {
                return Word[0].toUpperCase() + Word.substr(1);
            })
            .join(' ');
    };

    const onSubscribe = async (values: FormRes) => {
        showLoading('loadingApi');

    };

    return (
        <div className="footer-home">
            <div className="content-footer">
                <Row>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }}>
                        <div className="footer-left">
                            <label>Hà Nội</label>
                            <div className="address">
                                <span>
                                    <GoLocation />
                                </span>
                                <label>
                                    {toCapitalize(
                                        'Số 160 Khâm Thiên, Đống Đa, Hà Nội'
                                    )}
                                </label>
                            </div>
                            <div className="phone">
                                <span>
                                    <FiPhoneCall />
                                </span>
                                <label>038 300 7243</label>
                            </div>
                            <div className="social">
                                <label>
                                    {translates('home.footer.social')}
                                </label>
                                <div className="list-social">
                                    <span className="item-social">
                                        <TiSocialFacebook className="facebook" />
                                    </span>
                                    <span className="item-social">
                                        <TiSocialYoutube className="youtube" />
                                    </span>
                                    <span className="item-social">
                                        <TiSocialTwitter className="other" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }}>
                        <div className="footer-right">
                            <label>{translates('home.footer.title')}</label>
                            <p>{translates('home.footer.notification')}</p>
                            <div className="footer-form-register">
                                <Form onFinish={onSubscribe} form={form}>
                                    <Row gutter={[50, 0]}>
                                        <Col span={12}>
                                            <Item
                                                name="name"
                                                rules={[
                                                    {
                                                        whitespace: true,
                                                        required: true,
                                                        message: translates(
                                                            'home.formRegister.required',
                                                            {
                                                                label: translates(
                                                                    'home.formRegister.name'
                                                                )
                                                            }
                                                        )
                                                    }
                                                ]}
                                            >
                                                <Input
                                                    placeholder={toCapitalize(
                                                        translates(
                                                            'home.formRegister.name'
                                                        )
                                                    )}
                                                    className="input-item"
                                                    size="large"
                                                />
                                            </Item>
                                        </Col>
                                        <Col span={12}>
                                            <Item
                                                name="phone"
                                                rules={[
                                                    {
                                                        whitespace: true,
                                                        required: true,
                                                        message: translates(
                                                            'home.formRegister.required',
                                                            {
                                                                label: translates(
                                                                    'home.formRegister.phone'
                                                                )
                                                            }
                                                        )
                                                    },
                                                    {
                                                        pattern: new RegExp(
                                                            RegexUtils.RegexConstants.REGEX_PHONE
                                                        ),
                                                        message: translates(
                                                            'home.formRegister.formatError',
                                                            {
                                                                label: translates(
                                                                    'home.formRegister.phone'
                                                                )
                                                            }
                                                        )
                                                    }
                                                ]}
                                            >
                                                <Input
                                                    placeholder={toCapitalize(
                                                        translates(
                                                            'home.formRegister.phone'
                                                        )
                                                    )}
                                                    className="input-item"
                                                    size="large"
                                                />
                                            </Item>
                                        </Col>
                                        <Col span={12}>
                                            <Item
                                                name="email"
                                                rules={[
                                                    {
                                                        whitespace: true,
                                                        required: true,
                                                        message: translates(
                                                            'home.formRegister.required',
                                                            {
                                                                label: translates(
                                                                    'home.formRegister.email'
                                                                )
                                                            }
                                                        )
                                                    },
                                                    {
                                                        pattern: new RegExp(
                                                            RegexUtils.RegexConstants.REGEX_EMAIL
                                                        ),
                                                        message: translates(
                                                            'home.formRegister.formatError',
                                                            {
                                                                label: translates(
                                                                    'home.formRegister.email'
                                                                )
                                                            }
                                                        )
                                                    }
                                                ]}
                                            >
                                                <Input
                                                    placeholder={toCapitalize(
                                                        translates(
                                                            'home.formRegister.email'
                                                        )
                                                    )}
                                                    className="input-item"
                                                    size="large"
                                                />
                                            </Item>
                                        </Col>
                                        <Col span={12}>
                                            <Item
                                                name="branchId"
                                                rules={[
                                                    {
                                                        whitespace: true,
                                                        required: true,
                                                        message: translates(
                                                            'home.formRegister.required',
                                                            {
                                                                label: translates(
                                                                    'home.formRegister.facility'
                                                                )
                                                            }
                                                        )
                                                    }
                                                ]}
                                            >
                                                <Select
                                                    allowClear
                                                    placeholder={translates(
                                                        'home.formRegister.facility'
                                                    )}
                                                    className="select-item"
                                                    size="large"
                                                >
                                                    {["Hà Nội", "Nam Định"].map((e) => {
                                                        return (
                                                            <Option
                                                                key={e}
                                                                value={e}
                                                            >
                                                                {e}
                                                            </Option>
                                                        );
                                                    })}
                                                </Select>
                                            </Item>
                                        </Col>
                                    </Row>
                                    <Row className="row-btn-submit">
                                        <Col span={24}>
                                            <Item>
                                                <Button
                                                    className="btn-submit-form-register"
                                                    type="primary"
                                                    htmlType="submit"
                                                    size="large"
                                                >
                                                    {translates(
                                                        'home.formRegister.registerNow'
                                                    )}
                                                </Button>
                                            </Item>
                                        </Col>
                                    </Row>
                                </Form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        </div>
    );
}
