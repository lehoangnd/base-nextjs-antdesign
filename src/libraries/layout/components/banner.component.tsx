import { Carousel } from 'antd';
import { fetchBanners } from 'handler/home/home.service';
import { Banner } from 'handler/home/home.types';
import PageLoading from 'libraries/page-loading';
import React, { useEffect, useState } from 'react';
//@ts-ignore
import styles from '../layout.module.scss';

interface Props {
}

const ImageHeight = 500;

export default function BannerComponent(props: Props){
    const [banners, setBanners] = useState<Banner[]>([]);

    // lấy danh sách banner
    const fetchData = async (): Promise<void> => {
        const res = await fetchBanners();
        setBanners(res);
    };

    useEffect(() => {
        // didmount
        fetchData();
    }, []);

    return (
        <Carousel
            autoplay
            fade
            // useCSS
            autoplaySpeed={3000}
            speed={600}
            pauseOnHover={false}
            slidesToShow={1}
            slidesToScroll={1}
            dotPosition="left"
            // cssEase="linear"
            style={{ height: ImageHeight, overflow: 'hidden' }}
        >
            {banners.length === 0 && <PageLoading />}
            {banners.map((e) => {
                return (
                    <img
                        key={e._id}
                        src={e.image?.url}
                        alt="Amostudy image"
                        height={ImageHeight}
                        className={styles.bannerImage}
                    />
                );
            })}
        </Carousel>
    );
};
