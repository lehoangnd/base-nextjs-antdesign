import { MailOutlined } from '@ant-design/icons';
import { Button, Col, Layout, Row } from 'antd';
import UserContext from 'libraries/context/user.context';
import { useContext } from 'react';
import { translates } from 'res/languages';
import { showModalSignin, showModalSignup } from 'utils/common.utils';
// @ts-ignore
import styles from './layout.module.scss';

const { Header } = Layout;

interface Props {
}

function HeaderLayout(props: Props) {
    const { user } = useContext(UserContext);
    return (
        <Header className={styles.header}>
            <div className="main-content">
                <Row gutter={24} style={{ height: '100%' }}>
                    <Col xs={{ span: 24 }} sm={{ span: 10 }}>
                        <div className={styles.email}>
                            <a href="mailto:support@amostudy.vn">
                                <MailOutlined className={styles.icEmail} />
                                support@amostudy.vn
                            </a>
                        </div>
                    </Col>
                    <Col
                        xs={{ span: 24 }}
                        sm={{ span: 14 }}
                        className={`${styles.headerRight} header_right`}
                    >
                        <Button type="link" className={styles.email}>
                            <img src="/icNotify.png" width={30} />
                            {translates('notification')}
                        </Button>
                        <Button type="link" className={styles.email}>
                            <img src="/icHelp.png" width={30} />
                            {translates('help')}
                        </Button>
                        {/* {props.user && (
                        <Button
                            type="link"
                            className={styles.email}
                            onClick={props.onLogout}
                        >
                            <LogoutOutlined className={styles.icLogout} />
                            {translates('logout')}
                        </Button>
                    )} */}
                        {!user && (
                            <Button
                                type="link"
                                className={styles.email}
                                onClick={showModalSignin}
                            >
                                <img
                                    src="/icUser.png"
                                    width={15}
                                    className={styles.icUser}
                                />
                                {translates('signin')}
                            </Button>
                        )}
                        {!user && (
                            <Button
                                type="link"
                                className={styles.btnSignup}
                                onClick={showModalSignup}
                            >
                                <div className={styles.signup}>
                                    {translates('signup')}
                                </div>
                            </Button>
                        )}
                        <div className="social_contact_header">
                            <Button
                                type="link"
                                className={styles.icFb}
                                href="http://fb.com/lehoangnd"
                                target="_blank"
                            >
                                <img src="/social/icFb.png" width={30} />
                            </Button>
                            <Button
                                type="link"
                                className={styles.icFb}
                                href="http://fb.com/lehoangnd"
                                target="_blank"
                            >
                                <img src="/social/icGoogle.png" width={30} />
                            </Button>
                            <Button
                                type="link"
                                className={styles.icYoutube}
                                href="http://fb.com/lehoangnd"
                                target="_blank"
                            >
                                <img src="/social/icYoutube.png" width={30} />
                            </Button>
                        </div>
                    </Col>
                </Row>
            </div>
        </Header>
    );
}

export default HeaderLayout;
