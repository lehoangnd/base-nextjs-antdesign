import { CloseOutlined, MenuOutlined, SearchOutlined } from '@ant-design/icons';
import { Button, Col, Input, Row } from 'antd';
import UserContext from 'libraries/context/user.context';
import { useRouter } from 'next/dist/client/router';
import Link from 'next/link';
import Router from 'next/router';
import { useContext, useEffect, useState } from 'react';
import { translates } from 'res/languages';
import { showModalSignin } from 'utils/common.utils';
// @ts-ignore
import styles from './navbar.module.scss';

interface Props {
    isShowContent: (isShow: boolean) => void;
}

function MenuLayout(props: Props) {
    const router = useRouter();
    const [click, setClick] = useState<boolean>(false);
    const [scrolled, setScrolled] = useState(false);
    const [className, setClassName] = useState<string>('');
    const { totalOrder, user } = useContext(UserContext);


    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    useEffect(() => {
        props.isShowContent(click);
    }, [click]);

    useEffect(() => {
        if (scrolled) {
            setClassName(
                `${styles.navbarContainer} ${scrolled && styles.navbarScrolled}`
            );
        } else {
            setClassName(styles.navbarContainer);
        }
    }, [scrolled]);

    const checkCurrentPath = (path: string): boolean => {
        return router.pathname.includes(path);
    };

    const handleScroll = () => {
        const offset = window.scrollY;
        if (offset > 64) {
            setScrolled(true);
        } else {
            setScrolled(false);
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
    });

    const onGoPersonal = () => {
        if (!user) {
            showModalSignin();
        } else {
            Router.push('/personal')
        }
    }

    const onGoCart = () => {
        if (!user) {
            showModalSignin();
        } else {
            Router.push('/cart')
        }
    }

    return (
        <Row>
            <Col span={24} className={className}>
                <nav className={`main-content ${styles.navbar}`}>
                    <Link href="/">
                        <a className={styles.navbarLogo}>
                            <img
                                src="/logo/icLogo.png"
                                alt="Amostudy Logo"
                                height="80"
                            />
                        </a>
                    </Link>
                    <div className={styles.menuIcon} onClick={handleClick}>
                        {click ? (
                            <CloseOutlined className={styles.faTimes} />
                        ) : (
                                <MenuOutlined className={styles.faBars} />
                            )}
                    </div>
                    <ul
                        className={
                            click
                                ? `${styles.navMenu} ${styles.active}`
                                : styles.navMenu
                        }
                    >
                        <li className={styles.navItem}>
                            <Link href="/introduce">
                                <a
                                    className={styles.navLinks}
                                    onClick={closeMobileMenu}
                                >
                                    {translates('introduce')}
                                </a>
                            </Link>
                        </li>
                        <li className={styles.navItem}>
                            <Link href="/">
                                <a
                                    className={styles.navLinks}
                                    onClick={closeMobileMenu}
                                >
                                    {translates('document')}
                                </a>
                            </Link>
                        </li>

                        <li className={styles.navItem}>
                            <Link href="/become-teacher">
                                <a
                                    className={styles.navLinks}
                                    onClick={closeMobileMenu}
                                >
                                    {translates('becomeATeacher')}
                                </a>
                            </Link>
                        </li>

                        <li className={styles.navItem}>
                            <Link href="/">
                                <a
                                    className={styles.navLinks}
                                    onClick={closeMobileMenu}
                                >
                                    {translates('contact')}
                                </a>
                            </Link>
                        </li>
                        <li className={styles.navItem}>
                            <div className={`${styles.navLinks} ${styles.nonBorder}`}>
                                <Input
                                    size="large"
                                    placeholder={translates('search')}
                                    prefix={
                                        <SearchOutlined
                                            className={styles.iconInputSearch}
                                        />
                                    }
                                    className={styles.inputSearch}
                                />
                            </div>
                        </li>
                        <li className={styles.navItem}>
                            <Button type="link" onClick={onGoCart}>
                                <a
                                    className={`${styles.navLinks} ${styles.nonBorder} ${styles.cartView}`}
                                >
                                    <img src="/menu/icCart.png" width={26} />
                                    {totalOrder > 0 && <div className={styles.badgeView}>
                                        {totalOrder}
                                    </div>}
                                </a>
                            </Button>
                        </li>
                        <li className={styles.navItem}>
                            <Button type="link" style={{ padding: 0 }} onClick={onGoPersonal}>
                                <a
                                    className={`${styles.navLinks} ${styles.icUser} ${styles.nonBorder}`}
                                >
                                    <img src="/menu/icUser.png" width={30} />
                                </a>
                            </Button>
                        </li>
                    </ul>
                </nav>
            </Col>
        </Row>
    );
}

export default MenuLayout;
