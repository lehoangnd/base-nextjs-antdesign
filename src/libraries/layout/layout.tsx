import { Layout } from 'antd';
import ChangePasswordModal from 'libraries/modal/change-password.modal';
import ForgotPasswordModal from 'libraries/modal/forgot-password.modal';
import OtpModal from 'libraries/modal/otp.modal';
import SigninModal from 'libraries/modal/signin.modal';
import SignupModal from 'libraries/modal/signup.modal';
import Head from 'next/head';
import React, { CSSProperties, useState } from 'react';
import FooterLayout from './footer.layout';
import HeaderLayout from './header.layout';
import MenuLayout from './navbar-custom/navbar.layout';

interface Props {
    title?: string;
    children: any;
    style?: CSSProperties;
}

const { Content } = Layout;

export default function MainLayout(props: Props) {
    const [isShowContent, setIsShowContent] = useState<boolean>(false);
    return (
        <>
            <Head>
                <title>
                    {props.title ||
                        'Base NextJS - Created by leatech.vn team'}
                </title>
                <link
                    rel="apple-touch-icon"
                    type="image/jpeg"
                    sizes="180x180"
                    href="/favicon/favicon180.png"
                />
                <link
                    rel="icon"
                    type="image/jpeg"
                    sizes="32x32"
                    href="/favicon/favicon32.png"
                />
                <link
                    rel="icon"
                    type="image/jpeg"
                    sizes="16x16"
                    href="/favicon/favicon16.png"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <meta
                    name="description"
                    content="Base NextJS - Created by leatech.vn team"
                />
            </Head>

            <Layout>
                <HeaderLayout />
                <MenuLayout
                    isShowContent={(isShow) => setIsShowContent(isShow)}
                />
                
                <Content
                    className={`${isShowContent ? 'disableContent' : ''}`}
                    style={props.style}
                >
                    {props.children}
                </Content>
                
                
                <FooterLayout />
            </Layout>
            <SignupModal />
            <SigninModal />
            <ForgotPasswordModal />
            <OtpModal />
            <ChangePasswordModal />
        </>
    );
}
