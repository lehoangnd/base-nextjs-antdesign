export enum MediaType {
    PHOTO = 'PHOTO',
    VIDEO = 'VIDEO',
    FILE = 'FILE',
    AUDIO = 'AUDIO'
}

export interface Media {
    _id: string;
    url: string;
    width?: number;
    height?: number;
    type: MediaType;
}

export enum ActiveType {
    ACTIVE = 'ACTIVE',
    INACTIVE = 'INACTIVE'
}

export interface User {
    _id: string;
    name: string;
    dialCode: string;
    phone: string;
    email: string;
}

export interface MessageAndStatusRes {
    messageKey: string;
    status: boolean;
}

export interface Admin {
    _id: string;
    name: string;
    dialCode: string;
    phone: string;
    position: string;
    email: string;
    birthday: string;
    gender: string;
    createdAt: string;
    status: ActiveType;
    permissions: Permission[];
    role: AdminType;
}

export enum PermissionType {
    ALL = 'ALL',
    ONLY_VIEW = 'ONLY_VIEW',
    EDITOR = 'EDITOR',
    VIEW_ANALYTICS = 'VIEW_ANALYTICS',
    EXPORT_CSV = 'EXPORT_CSV',
    ADD_STAFF = 'ADD_STAFF'
}

export enum AdminType {
    STAFF = 'STAFF',
    ADMIN = 'ADMIN'
}


export interface Permission {
    name: string;
    type: PermissionType;
}
export interface DataTotalRes<T> {
    data: T[];
    total: number;
}

// Thêm 1 type thì khai báo tên trong LessonTypeName
export enum TypeExercise {
    REPLACE_UNDERLINE_WORD = 'REPLACE_UNDERLINE_WORD', // Thay thế từ gạch chân
    MATCH_COLUMN_ABC = 'MATCH_COLUMN_ABC', // Nối từ trong các cột A B C
    MATCH_COLUMN_AB  = 'MATCH_COLUMN_AB', // Nối từ trong các cột A B
    MATCH_UNDERLINE_WORD_WITH_IT_MEAN = 'MATCH_UNDERLINE_WORD_WITH_IT_MEAN', // Chọn nghĩa của từ được gạch chân trong đoạn văn
    CHOOSE_WORDS_IN_PARAGRAPH = 'CHOOSE_WORDS_IN_PARAGRAPH', // Chọn đáp án đúng trong đoạn văn
    CHOOSE_WORDS_IN_SENTENCES = 'CHOOSE_WORDS_IN_SENTENCES', // Chọn đáp án đúng trong câu
    FILL_IN_THE_BLANKS = 'FILL_IN_THE_BLANKS', // Điền từ vào chỗ trống
}
