export interface FormRegister {
    name: string;
    phone: string;
    email: string;
    facility: string;
}